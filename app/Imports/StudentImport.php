<?php

namespace App\Imports;

use App\Models\Student;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class StudentImport implements ToModel, WithHeadingRow, WithValidation
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {   
        return new Student([
            'name'         => $row['name'],
            'student_id'    => $row['student_number'], 
            'grade'         => $row['year_level'], 
            'strand'        => $row['strand'], 
        ]);
    }
    
    public function rules(): array
    {
        return [
            'student_number' => Rule::unique('students', 'student_id'),
        ];
    }
}
