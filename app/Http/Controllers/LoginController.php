<?php

namespace App\Http\Controllers;

use App\Models\Student;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Models\Position;
use App\Models\Candidate;
use App\Models\VoteRecord;


class LoginController extends Controller
{
    public function index(Request $request)
    {
        return view('student_login');
    }

    public function login_student(Request $request)
    {
        $student_id = $request->username;
        $find = Student::where('student_id', $student_id)->first();
        
        if($find){
            $request->session()->put('student_login',$student_id);
            $request->session()->put('student_name',$find->name);
            $request->session()->put('grade',$find->grade);
            $request->session()->put('strand',$find->strand);

            return redirect('vote');
            
        }else{
            return redirect('/')->with(['error' => 'Error! Cannot find Username.']);
        }
    }

    public function admin_index(Request $request)
    {
        return view('admin_login');
    }

    public function login_admin(Request $request)
    {
        $username = $request->username;
        $password = $request->password;
        $find = User::where('username', $username)->first();
        if($find){
            if (Hash::check($password, $find->password)) {
                //set session variables
                $request->session()->put('login_name',$username);
                $request->session()->put('user_id',$find->id);

                return redirect('admin_dashboard');
            }else{
                return redirect('admin_login')->with(['error' => 'Error! Username/Password does not match.']);
            }
        }else{
            return redirect('admin_login')->with(['error' => 'Error! Username/Password does not match.']);
        }
    }
}
