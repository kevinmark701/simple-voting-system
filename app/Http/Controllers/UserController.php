<?php

namespace App\Http\Controllers;

use App\Models\Candidate;
use App\Models\Config;
use App\Models\Position;
use App\Models\Student;
use App\Models\VoteRecord;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        //get all the positions 
        $positions = Position::get();

        //get all the candidates running in the position
        $candidate = function ($position_id){
            return Candidate::where('position_id', $position_id)->get();
        };

        //get count of number of students
        $student_count = Student::count();

        //get election status if open
        $status = Config::where('name', 'election_status')->first();

        $data = [
            'positions'         => $positions,
            'candidates'        => $candidate,
            'student_count'     => $student_count,
            'election_status'   => $status
        ];

        return view('admin_dashboard', $data);
    }

    public function get_tally()
    {
        $result = VoteRecord::selectRaw('candidate_id as candidate, count(candidate_id) as num_rows')->groupBy('candidate_id')->get();
        return json_encode($result);
        exit(0);
    }

    public function get_students_voted()
    {
        return json_encode(VoteRecord::distinct('student_id')->count());
        exit(0);
    }

    public function logout_admin(Request $request)
    {
        $request->session()->forget('login_name');
        $request->session()->forget('user_id');

        return redirect('admin_login');
    }

    public function update_election_status(Request $request)
    {
        $value = $request->value;

        $config = Config::where('name', 'election_status')->first();
       
        $config->value = $value;
        return $config->save();

    }

   
}
