<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Student;
use App\Models\VoteRecord;
use App\Models\Position;
use App\Models\Candidate;
use App\Models\Config;

class StudentController extends Controller
{
    public function index(Request $request)
    {

        $students = function($grade, $strand){
            return Student::where('grade',$grade)
                ->where('strand', $strand)
                ->orderBy('name')
                ->get();
        };

        $grade_strand = Student::select('grade', 'strand')
            ->distinct('grade', 'strand')
            ->orderBy('grade')
            ->orderBy('strand')
            ->get();
        
        return view('student_list', ['students' => $students, 'grade_strands' => $grade_strand]);
    }

    public function submit_vote(Request $request)
    {   
        $status = Config::select('value')->where('name', 'election_status')->first();
        
         //check if election is open
        if($status->value == "0"){
            return redirect('vote');
        }

        //check if student has logged in
        else if(session()->has('student_login') && session()->has('student_name')){
        }else{
            return redirect('/');
        }


        //check if student has allready voted
        if( count(VoteRecord::where('student_id', session()->get('student_login'))->get()) ){
            return redirect('vote_finished')->with('warning', 'Sorry! You have submitted your votes already.');
        }

        $votes = $request->all();

        foreach ($votes as $k => $vote) {
            if(!is_numeric($k)) continue;

            $record = new VoteRecord;

            $record->student_id = session()->get('student_login');
            $record->position_id = $k;
            $record->candidate_id = $vote;
            $record->save();

        }
        
        return redirect('vote')->with('success', 'Thank you! You have successfully submitted your votes.');
    }

    public function vote_finished(Request $request)
    {
        return view('finished_voting');
    }

    public function logout_student(Request $request)
    {
        $request->session()->forget('student_login');
        $request->session()->forget('student_name');
        $request->session()->forget('grade');
        $request->session()->forget('strand');

        return redirect('/');
    }

    public function student_vote()
    {
        //check if the student has submitted a vote allready
        $vote_record = VoteRecord::where('student_id', session()->get('student_login'))->get();
        $is_finished = false;

        if( count( $vote_record ) ){
            // return view('finished_voting');
            session()->flash('success', 'Thank you! You have successfully submitted your votes already.');
            $data['vote_record'] = $vote_record;

        }


        //check if election is open
        $data['election_status'] = '0';
        $status = Config::select('value')->where('name', 'election_status')->first();
        if(!empty($status)){
            $data['election_status'] = $status->value;
           
        }

        //get all the positions 
        $positions = Position::get();

        //get all the candidates running in the position
        $candidate = function ($position_id){
            return Candidate::where('position_id', $position_id)->get();
        };

        $data['positions'] = $positions;
        $data['candidates'] = $candidate;

        return view('student_vote', $data);
    }
    
}
