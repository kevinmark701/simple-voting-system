<?php

use App\Http\Controllers\LoginController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ImportExcelController;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [LoginController::class, 'index']);
Route::post('login_student', [LoginController::class, "login_student"]);


Route::get('admin_login', [LoginController::class, 'admin_index']);
Route::post('login_admin', [LoginController::class, 'login_admin']);

Route::get('admin_dashboard', [UserController::class, 'index']);

Route::get('import_excel', [ImportExcelController::class, 'index']);
Route::post('import_excel/import', [ImportExcelController::class, 'import']);

Route::get('student_list', [StudentController::class, 'index']);

Route::get('student_vote', [StudentController::class, 'index']);
Route::post('submit_vote', [StudentController::class, 'submit_vote']);

Route::get('vote_finished', [StudentController::class, 'vote_finished']);

Route::get('logout_student', [StudentController::class, 'logout_student']);
Route::get('logout_admin', [UserController::class, 'logout_admin']);

Route::get('get_tally', [UserController::class, 'get_tally']);
Route::get('get_students_voted', [UserController::class, 'get_students_voted']);

Route::get('vote', [StudentController::class, 'student_vote']);


Route::get('update_election_status', [UserController::class, 'update_election_status']);