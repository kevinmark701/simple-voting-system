<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Voting System</title>

    <link rel="stylesheet" href={{ asset('css/bootstrap.min.css') }}>

    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/jquery-3.6.0.min.js') }}"></script>

    <style>
        footer {
            position: fixed;
            padding: 10px 10px 0px 10px;
            bottom: 0;
            width: 100%;
            /* Height of the footer*/ 
            height: 50px;
            background: grey;
        }
    </style>
</head>
<body>
    <header class="d-flex flex-wrap justify-content-center py-3 mb-4 border-bottom">
        <a href="#" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-dark text-decoration-none">
            <svg class="bi me-2" width="40" height="32"><use xlink:href="#bootstrap"></use></svg>
            <span class="fs-4">Welcome {{ request()->session()->get('login_name') }}</span>
        </a>

        <ul class="nav nav-pills">
            <!-- <li class="nav-item"><a href="#" class="nav-link">Upload Student List</a></li>
            <li class="nav-item"><a href="#" class="nav-link">Student List</a></li>-->
            <li class="nav-item"><a href="logout_admin" class="nav-link">Logout</a></li>
        </ul>
    </header>
    <div class="container-fluid">
        <div class="row">
            <div class="col-2">
                <div class="list-group">
                    <a href="admin_dashboard" type="button" class="list-group-item list-group-item-action" aria-current="true">Dashboard</a>
                    <a href="import_excel" type="button" class="list-group-item list-group-item-action" aria-current="true">Upload Student List</a>
                    <a href="student_list" class="list-group-item list-group-item-action">Student List</a>
                </div>
            </div>
            <div class="col">
                @yield('content')
            </div>
        </div>
    </div>


    <footer class="footer mt-auto py-3 bg-light">
        <div class="container">
            <span class="text-muted">Created by: K.Pecdasen (Oct-2021)</span>
        </div>
    </footer>

</body>
</html>