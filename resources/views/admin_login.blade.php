@extends('app')

@section('content')
    <br>


    <div class="row justify-content-md-center">
        <div class="col-md-6">
            <h1>Admin Login</h1>

            @if(session()->has('error'))
                <div class="alert alert-danger">
                    {{ session()->get('error') }}
                </div>
            @endif

            <form method="post" action="login_admin">
                {{ csrf_field() }}

                <div class="input-group mb-3">
                    <span class="input-group-text" id="basic-addon1">Username: </span>
                    <input type="text" name="username" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1" autocomplete="off" aria-autocomplete="off">
                </div>

                <div class="input-group mb-3">
                    <span class="input-group-text" id="basic-addon2">Password: </span>
                    <input type="password" name="password" placeholder="Password" aria-label="Password" aria-describedby="basic-addon2">
                </div>
                
                <br/>
                <input type="submit" class="btn btn-success" />
            </form>
        </div>
    </div>
@stop