<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Voting System</title>

    <link rel="stylesheet" href={{ asset('css/bootstrap.min.css') }}>

    <style>
        footer {
            position: fixed;
            padding: 10px 10px 0px 10px;
            bottom: 0;
            width: 100%;
            /* Height of the footer*/ 
            height: 50px;
            background: grey;
        }
    </style>

    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/jquery-3.6.0.min.js') }}"></script>
</head>
<body>
    <div class="row">
        <header class="d-flex flex-wrap justify-content-center py-3 mb-4 border-bottom">
            <a href="#" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-dark text-decoration-none">
                <svg class="bi me-2" width="40" height="32"><use xlink:href="#bootstrap"></use></svg>
                <span class="fs-4">Welcome {{ request()->session()->get('student_name') }}</span>
            </a>

            <ul class="nav nav-pills bg-seconday">
                <!-- <li class="nav-item"><a href="#" class="nav-link">Upload Student List</a></li>
                <li class="nav-item"><a href="#" class="nav-link">Student List</a></li> -->
                <li class="nav-item"><a href="logout_student" class="nav-link">Logout</a></li>
            </ul>
        </header>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-1">
                {{-- <div class="list-group">
                    <a href="import_excel" type="button" class="list-group-item list-group-item-action" aria-current="true">Upload Student List</a>
                    <a href="student_list" class="list-group-item list-group-item-action">Student List</a>
                    <a href="vote_result" class="list-group-item list-group-item-action">Vote Result</a>
                </div> --}}
            </div>
            <div class="col">
                @yield('content')
            </div>
            <div class="col-1"></div>
        </div>

        <div class="row">
            <footer class="footer mt-auto py-3 bg-light">
                <div class="container">
                    <span class="text-muted">Created by: K.Pecdasen (Oct-2021)</span>
                </div>
            </footer>
        </div>
    </div>

    

</body>
</html>