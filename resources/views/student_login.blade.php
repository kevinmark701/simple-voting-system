@extends('app')

@section('content')
    <br>
    <h1>Student Login</h1>

    @if(session()->has('error'))
        <div class="alert alert-danger">
            {{ session()->get('error') }}
        </div>
    @endif

    <form method="post" action="login_student">
        {{ csrf_field() }}

        <div class="input-group mb-3">
            <span class="input-group-text" id="basic-addon1">Username: </span>
            <input type="text" name="username" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1" autocomplete="off">
        </div>
        
        <br/>
        <input type="submit" class="btn btn-success" />
    </form>
@stop