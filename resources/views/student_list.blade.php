@extends('layout')

@section('content')
    <style>
        ol {
        columns: 2;
        -webkit-columns: 2;
        -moz-columns: 2;
        }
    </style>


    <h2>Student List</h2>
    @foreach ($grade_strands as $key => $gs)
        <div class="accordion" id="accordionExample">
            <div class="accordion-item">
            <h2 class="accordion-header" id="headingOne">
                <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne{{ $key }}" aria-expanded="true" aria-controls="collapseOne">
                {{ $gs->grade."-".$gs->strand }}
                </button>
            </h2>
            <div id="collapseOne{{ $key }}" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                <div class="accordion-body" >
                    <ol>
                        @foreach ($students($gs->grade, $gs->strand) as $student)
                            <li>{{ $student->name." "}}<u>{{ $student->student_id }}</u> </li>
                        @endforeach
                    </ol>
                </div>
            </div>
            </div>
        </div>
    @endforeach

    <br>
    <br>
    <br>
@stop