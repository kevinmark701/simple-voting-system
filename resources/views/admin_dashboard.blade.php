@extends('layout')

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />

    <style>
        /* ol {
        columns: 2;
        -webkit-columns: 2;
        -moz-columns: 2;
        } */
        ol li{
            float: left;
            width: 50%;//helps to determine number of columns, for instance 33.3% displays 3 columns
        }
        ol{
            list-style-type: disc;
        }

        .bordered {
            border: 1px solid black;
        }
        .xy{
            width: 5em;
        }
    </style>

    <div class="row">
        <div class="col-9 mb-5 pb-5">
            <div class="btn btn-lg btn-danger" id='btn_enclose'>
                <div class="form-check form-switch">
                    <input class="form-check-input" type="checkbox" id="change_status">
                    <label class="form-check-label" for="change_status" id="status_msg">Election is closed.</label>
                </div>
            </div>
            <br>
            <br>

            <h3>Tally</h3>

            <ol class="mb-2">
                @foreach ($positions as $pos)
                    <li>
                        <span>{{ $pos->position }}</span><br>
                        <table width="80%">
                        @foreach ($candidates($pos->id) as $cand)
                            <tr>
                                <td>&emsp;</td>
                                <td class="bordered">&emsp;{{ $cand->name }}&emsp;</td>
                                <td class="bordered xy"><span id="{{ $cand->id }}"></span></td>
                            </tr>
                        @endforeach
                        </table>
                    </li>
                @endforeach
            </ol>

        </div>

        <div class="col" style="border-left: 1px solid #000;">
            <h3>Progress</h3>
            <br>
            <h4><span> <span id="student_voted"></span> out of {{ $student_count }} has finished voting.</span></h4>
            <div class="progress">
                <div class="progress-bar bg-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
        </div>
    </div>


    <script>
        $(function(){

            function set_variables(){
                $.ajax({
                    url: 'get_tally',
                    type: 'get',
                    dataType: 'json',
                    success: function(d){
                        $.each(d, function(i, val){
                            $('#'+val.candidate).text(val.num_rows);
                        });
                    }
                });

                $.ajax({
                    url: 'get_students_voted',
                    type: 'get',
                    dataType: 'json',
                    success: function(d){
                        $('#student_voted').text(d);
                        calc = ( d/{{ $student_count }} ) * 100
                        $('.progress-bar').css('width', calc+'%' ).attr('aria-valuenow', calc );
                    }
                });
            };

            set_variables();

            setInterval(function(){
                set_variables();
            }, 5000);

            //set the initial election status indicators
            var is_checked = '{{ $election_status->value }}';
            $('#btn_enclose').removeClass('btn-light').removeClass('btn-danger');

            if(is_checked == 0){
                $('#btn_enclose').removeClass('btn-light').removeClass('btn-danger').addClass('btn-danger');
                $('#status_msg').text("Election is closed.");
                $('#change_status').prop('checked', false);
            }else{
                $('#btn_enclose').removeClass('btn-light').removeClass('btn-danger').addClass('btn-light');
                $('#status_msg').text("Election is open.");
                $('#change_status').prop('checked', true);
            }

            //update config, open/close election
            $('#change_status').on('change', function(){
                var is_checked = $(this).is(':checked') ? 1 : 0;

                $.ajax({
                    url: 'update_election_status',
                    type: 'get',
                    dataType: 'json',
                    data: {'value': is_checked},
                    success: function(d){
                        console.log(d == 1);
                        if(d == 1){
                            var text = "Election is closed.";
                            var classx = 'btn-danger';
                            
                            if(is_checked){
                                var text = "Election is open.";
                                var classx = 'btn-light';
                            }
                            
                            
                            $('#btn_enclose').removeClass('btn-light').removeClass('btn-danger').addClass(classx);
                            $('#status_msg').text(text);
                        }
                    }
                });
                
            });

        });
    </script>
@stop