@extends('student_layout')

@section('content')
    <div>
        @if (session()->has('warning'))
            <h3 class="alert alert-warning">{{ session()->get('warning') }}</h3>
        @else
            <h3 class="alert alert-success">Thank you! You have successfully submitted your votes.</h3>
        @endif
        
    </div>
@stop