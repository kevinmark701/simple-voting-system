@extends('student_layout')

@section('content')

    @if (session()->has('success'))
        <h3 class="alert alert-success">{{ session()->get('success') }}</h3>
    @elseif (isset($election_status) && $election_status != '1')    
        <h3 class="alert alert-danger">Sorry! Election is closed for now.</h3>
    @else
        <h3 class="alert alert-warning">Please choose your candidate wisely, then click the submit button afterwards.</h3>
    @endif
    
    <div class="col-6">


        <form action="submit_vote" method="post">
            @csrf
            @foreach ($positions as $pos)
                {{-- Check if this student is qualified to vote for the position --}}

                @if ( 
                        ( empty($pos->grade) && empty($pos->strand) ) ||
                        ( !empty($pos->grade) && !empty($pos->strand) && $pos->grade == session()->get('grade') && $pos->strand == session()->get('strand') ) || 
                        ( !empty($pos->grade) && empty($pos->strand) && $pos->grade == session()->get('grade') )
                    )

                    {{-- Show text instead of select when student has already voted --}}
                    <div class="input-group mb-2">
                        <span class="input-group-text" id="basic-addon{{ $pos->id }}">{{ $pos->position }} :</span>

                        @if( isset($vote_record) )
                            @foreach ($candidates($pos->id) as $k => $cand )
                                    @foreach ($vote_record as $vt)
                                        @if (
                                            $vt->student_id == session()->get('student_login') &&
                                            $vt->position_id == $pos->id && 
                                            $vt->candidate_id == $cand->id
                                        )
                                            <input type="text" name="{{ $pos->id }}" class="form-control" aria-describedby="basic-addon{{ $pos->id }}" value="{{ $cand->name }}" disabled/>
                                            @break
                                        @endif

                                    @endforeach
                            @endforeach
                        
                        @else
                            <select name="{{ $pos->id }}" class="form-control" required aria-describedby="basic-addon1" {{ isset($election_status) && $election_status != '1' ? 'disabled' : '' }}>
                                <option value="" ></option>
                                @foreach ($candidates($pos->id) as $cand)
                                    <option class="bg-secondary" value="{{ $cand->id }}">{{ $cand->name }}</option>
                                @endforeach
                            </select>
                        @endif
                            
                        
                    </div>
                    
                @endif

                
            @endforeach

            <button class="btn btn-success btn-lg" {{ isset($vote_record) ? 'disabled' : '' }}>Submit</button>
        </form>
        
    </div>
@stop